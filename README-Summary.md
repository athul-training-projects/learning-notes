# Learning Notes

**The notes are about**

>file name: Data-structure-summary.pdf

_**Data Structures**_ 
- List (Singly linked, Doubly linked)
- Queue
- Stack
- Array
- Tree

> file name:Algorithm-summary.pdf

_**Algorithm**_
- Sort - Bubble Sort, Merge Sort, Quick Sort
- Search
- Tree Traversal
- Filtering

>file name: maths-summary.pdf

**_Maths_**
- Statistics - Min, Max, Mode, Median, Sum, Average
- Matrix - Addition, Multiplication
- Set - Union,  Intersection, Difference
> file name:network-summary.pdf

**_Network_**
- TCP
- UDP
- HTTP








