# Heading
# H1
## H6
### H5
#### H4
##### h2
###### h6

# Bold
# Bold Font

# italic font
_italic font_

# bolckquote
> bolckquote

# ordered list
1. ordered list1
2. ordered list2
3. ordered list3

# unordered list
- unordered list1
- unordered list2
- unordered list3

# code
```
<?php
echo "code";
?>
```

# horizontal rule
------------

# Link
[gitlab](https://gitlab.com/athul-training-projects/learning-notes "gitlab")

# Image
![my image](https://thumbs.dreamstime.com/z/sea-water-ocean-wave-surfing-surface-colorful-vibrant-sunset-barrel-shape-124362369.jpg)

# Table
|row1,coloumn1|row1,coloumn2|
|-------------|-------------|
|row2,coloumn1|row2,column2|

# Fenced Code Block
```
{
"first name":"athul",
            "second name":"vj"
}            
```
# Task List
- [x] checked checkbox
- [ ] unchecked checkbox
